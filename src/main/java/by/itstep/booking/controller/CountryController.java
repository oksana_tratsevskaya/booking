package by.itstep.booking.controller;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class CountryController {

    private final CountryService countryService;

    @PostMapping(path = "/country")
    public CountryDto create(
            @Valid
            @RequestBody CountryCreateDto dto
    ){
        return countryService.create(dto);
    }

    @PutMapping(path = "/country")
    public CountryDto update(
            @Valid
            @RequestBody CountryUpdateDto dto
    ){
        return countryService.update(dto);
    }

    @GetMapping(path = "/country/{id}")
    public CountryDto findById(@PathVariable Integer id){
        return countryService.findById(id);
    }

    @GetMapping(path = "/country")
    public Page<CountryDto> findAll(
            @RequestParam int page,
            @RequestParam int size
    ){
        return  countryService.findAll(page, size);
    }

    @DeleteMapping(path = "/country/{id}")
    public void delete(@PathVariable Integer id){
        countryService.deleteById(id);
    }
}
