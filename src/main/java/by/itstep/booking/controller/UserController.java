package by.itstep.booking.controller;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(path = "/users")
    public UserFullDto create(
            @Valid @RequestBody UserCreteDto dto
    ) {
        return userService.create(dto);
    }

    @PutMapping(path = "/users")
    public UserFullDto update(
            @Valid
            @RequestBody UserUpdateDto dto
    ){
        return userService.update(dto);
    }

    @GetMapping(path = "/users/{id}")
    public UserFullDto findById(@PathVariable Integer id){
        return userService.findById(id);
    }

    @GetMapping(path = "/users")
    public Page<UserFullDto> findAll(
            @RequestParam int page,
            @RequestParam  int size
    ){
        return userService.findAll(page, size);
    }

    @DeleteMapping(path = "/users/{id}")
    public void deleteById(@PathVariable Integer id){
        userService.deleteById(id);
    }

    @PutMapping(path =  "/users/changePassword")
    public void changePassword(
            @Valid
            @RequestBody ChangeUserPasswordDto dto
    ){
        userService.changePassword(dto);
    }

    @PutMapping(path = "/users/role")
    public UserFullDto changeRole(
            @Valid
            @RequestBody ChangeUserRoleDto dto
    ){
        return userService.changeRole(dto);
    }


    @PutMapping(path = "/users/{id}/blocked")
    public void blocked(
            @Valid
            @PathVariable Integer id
    )   throws Exception {
        userService.blocked(id);
    }

    @PutMapping(path = "/users/updateBalance")
    public UserFullDto updateBalance(@RequestBody UpdateBalanceDto dto){
        return userService.updateBalance(dto);
    }
}
