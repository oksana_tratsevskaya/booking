package by.itstep.booking.controller;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.select.AvailableHotelsRequest;
import by.itstep.booking.dto.select.AvailableHotelsResponse;
import by.itstep.booking.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;

    @PostMapping(path = "/hotels")
    public HotelFullDto create(
            @Valid
            @RequestBody HotelCreateDto dto
    ) {
        return hotelService.create(dto);
    }

    @PutMapping(path = "/hotels")
    public HotelFullDto update(
            @Valid
            @RequestBody HotelUpdateDto dto
    ) {
        return hotelService.update(dto);
    }

    @GetMapping(path = "/hotels/{id}")
    public HotelFullDto findById(@PathVariable Integer id) {
        return hotelService.findById(id);
    }

    @GetMapping(path = "/hotels")
    public Page<HotelFullDto> findAll(
            @RequestParam int page,
            @RequestParam int size
    ) {
        return hotelService.findAll(page, size);
    }

    @PostMapping(path = "/hotels/available")
    public List<AvailableHotelsResponse> findAvailableHotels(
            @Valid
            @RequestBody AvailableHotelsRequest dto
    ) {
        return hotelService.findAvailableHotels(dto);
    }

    @DeleteMapping(path = "/hotels/{id}")
    public void deleteById(@PathVariable Integer id) {
        hotelService.deleteById(id);
    }
}
