package by.itstep.booking.controller;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.service.BookingService;
import liquibase.pro.packaged.G;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @PostMapping(path = "/booking")
    public BookingFullDto create (
            @Valid
            @RequestBody BookingCreateDto dto
    ){
        return bookingService.create(dto);
    }

    @GetMapping(path = "/booking/{id}")
    public BookingFullDto findById (@PathVariable Integer id){
        return bookingService.findById(id);
    }

    @GetMapping(path = "/booking_user_id")
    public List<BookingFullDto> findByUserId(@RequestParam Integer userId){
        return bookingService.findByUserId(userId);
    }

    @GetMapping(path = "/booking")
    public Page<BookingFullDto> findAll(
            @RequestParam int page,
            @RequestParam int size
    ){
        return bookingService.findAll(page, size);
    }

    @DeleteMapping(path = "/booking/{id}")
    public void deleteById(@PathVariable Integer id){
        bookingService.deleteById(id);
    }

}
