package by.itstep.booking.controller;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;

    @PostMapping(path = "/rooms")
    public RoomFullDto create(
            @Valid
            @RequestBody RoomCreateDto dto
    ){
        return roomService.create(dto);
    }

    @PutMapping(path = "/rooms")
    public RoomFullDto update(
            @Valid
            @RequestBody RoomUpdateDto dto
    ){
        return roomService.update(dto);
    }

    @GetMapping(path = "/rooms/{id}")
    public RoomFullDto findById(@PathVariable Integer id){
        return roomService.findById(id);
    }

    @GetMapping(path = "/rooms")
    public Page<RoomFullDto> findAll(
            @RequestParam int page,
            @RequestParam int size
    ) {
        return roomService.findAll(page, size);
    }

    @DeleteMapping(path = "/rooms/{id}")
    public void deleteById(@PathVariable Integer id){
        roomService.deleteById(id);
    }
}
