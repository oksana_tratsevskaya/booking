package by.itstep.booking.mapper;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.select.AvailableHotelsResponse;
import by.itstep.booking.entity.HotelEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HotelMapper {

    HotelFullDto map(HotelEntity entity);

    HotelEntity map(HotelCreateDto dto);

    List<HotelShortDto> map(List<HotelEntity> entities);

    List<AvailableHotelsResponse> mapAvailableHotels(List<HotelEntity> entities);

}
