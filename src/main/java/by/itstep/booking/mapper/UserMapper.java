package by.itstep.booking.mapper;

import by.itstep.booking.dto.user.UserCreteDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    UserEntity map(UserCreteDto dto);

    List<UserFullDto> map(List<UserEntity> entities);


}
