package by.itstep.booking.mapper;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.entity.RoomEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoomMapper {

    RoomEntity map(RoomCreateDto dto);

    RoomFullDto map(RoomEntity entity);

    RoomShortDto mapShortDto(RoomEntity entity);

    List<RoomShortDto> map(List<RoomEntity> entities);

}
