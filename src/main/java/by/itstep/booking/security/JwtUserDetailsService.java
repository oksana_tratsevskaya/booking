package by.itstep.booking.security;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UserEntity user = userRepository
                .findByEmail(email)
                .orElseThrow(() -> new RuntimeException("JwtUserDetailsService ->>>> User was not found by email"));

        return new JwtUserDetails(
                user.getEmail(),
                user.getPassword(),
                user.getBlocked(),
                Arrays.asList(user.getRole()));
    }
}
