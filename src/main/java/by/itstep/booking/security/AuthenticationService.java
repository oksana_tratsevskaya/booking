package by.itstep.booking.security;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public UserEntity getAuthenticationUser() {
        Authentication authentication  = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            return null;
        }

        return userRepository.findByEmail(authentication.getName()).orElseThrow(RuntimeException::new);
    }
}
