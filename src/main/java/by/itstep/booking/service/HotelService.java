package by.itstep.booking.service;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.select.AvailableHotelsRequest;
import by.itstep.booking.dto.select.AvailableHotelsResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface HotelService {

    HotelFullDto create(HotelCreateDto dto);

    HotelFullDto update(HotelUpdateDto dto);

    HotelFullDto findById(int id);

    Page<HotelFullDto> findAll(int page, int size);

    void deleteById(int id);

    List <AvailableHotelsResponse> findAvailableHotels(AvailableHotelsRequest dto);

}
