package by.itstep.booking.service.impl;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValueIsTakenException;
import by.itstep.booking.mapper.RoomMapper;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomMapper roomMapper;
    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;
    private final BookingService bookingService;


    @Override
    public RoomFullDto create(RoomCreateDto dto) {
        RoomEntity entityToCreate = roomMapper.map(dto);
        entityToCreate.setHotel(
                hotelRepository
                .findById(dto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "CountryEntity was not found"))
        );

        RoomEntity roomCreated = roomRepository.save(entityToCreate);

        log.info("Room was added in BD");

        return roomMapper.map(roomCreated);
    }

    @Override
    public RoomFullDto update(RoomUpdateDto dto) {
        RoomEntity entityToUpdate = roomRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "RoomEntity was not found by id: " + dto.getId())
                );

        entityToUpdate.setSeatNumber(dto.getSeatNumber());

        log.info("Room with id: " + entityToUpdate.getId() + " was updated");

        return roomMapper.map(entityToUpdate);
    }

    @Override
    public RoomFullDto findById(int id) {
        RoomFullDto foundDTO = roomRepository
                .findById(id)
                .map(roomMapper::map)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "RoomEntity was not found by id: " + id)
                );

        log.info("Room with id: " + id + " was found");

        return foundDTO;
    }

    @Override
    public Page<RoomFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Page<RoomEntity> foundRoomEntities= roomRepository.findAll(pageable);

        Page<RoomFullDto> foundRooms = foundRoomEntities.map(roomEntity -> roomMapper.map(roomEntity));

        log.info("Rooms have been found");

        return foundRooms;
    }

    @Override
    public void deleteById(int id) {
        RoomEntity foundRoom = roomRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "RoomEntity was not found by id: " + id)
                );

        List<BookingEntity> bookingOfRooms = foundRoom.getBookings();
        if (bookingOfRooms == null) {
            log.info("No room reservation");
        } else {
            for (BookingEntity entity : bookingOfRooms) {
                bookingService.deleteById(entity.getId());
                log.info("Booking was successfully deleted");
            }
        }

        foundRoom.setDeletedAt(Instant.now());
        roomRepository.save(foundRoom);
    }
}
