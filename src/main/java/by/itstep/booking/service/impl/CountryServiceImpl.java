package by.itstep.booking.service.impl;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValueIsTakenException;
import by.itstep.booking.mapper.CountryMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.service.CountryService;
import by.itstep.booking.service.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryMapper countryMapper;
    private final CountryRepository countryRepository;
    private final HotelService hotelService;

    @Override
    @Transactional
    public CountryDto create(CountryCreateDto dto) {

        CountryEntity countryToCreate = countryMapper.map(dto);
        Optional<CountryEntity> countryWithSameName = countryRepository.findByName(countryToCreate.getName());

        if (countryWithSameName.isPresent()) {
            throw new UniqueValueIsTakenException(
                    "The country name is already in the database"
            );
        }

        CountryEntity countryCreated = countryRepository.save(countryToCreate);

        log.info("Country was added in BD");

        return countryMapper.map(countryCreated);
    }

    @Override
    @Transactional
    public CountryDto update(CountryUpdateDto dto) {

        CountryEntity countryToUpdate = countryRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "CountryEntity was not found by id: " + dto.getId())
                );

        countryToUpdate.setName(dto.getName());
        CountryEntity countryUpdated = countryRepository.save(countryToUpdate);

        log.info("Country was updated");

        return countryMapper.map(countryUpdated);
    }

    @Override
    @Transactional
    public CountryDto findById(int id) {

        CountryDto foundEntity = countryRepository
                .findById(id)
                .map(country ->countryMapper.map(country))
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "CountryServiceImpl -> CountryEntity was not found by id: " + id)
                );

        log.info("Country was found with #" + id);

        return foundEntity;
    }

    @Override
    @Transactional
    public Page<CountryDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Page<CountryEntity> countries = countryRepository.findAll(pageable);

        Page<CountryDto> dtos = countries.map(countryMapper::map);
        log.info("Countries successfully found");

        return dtos;
    }

    @Override
    @Transactional
    public void deleteById(int id) {

        CountryEntity foundCountry = countryRepository
                .findById(id)
                .orElseThrow(()-> new AppEntityNotFoundException(
                        "CountryEntity was not found by id: " + id)
                );

        List<HotelEntity> hotelOfCountry = foundCountry.getHotels();
        if (hotelOfCountry == null) {
            log.info("Not hotel's rooms");
        } else {
            for (HotelEntity entity : hotelOfCountry) {
                hotelService.deleteById(entity.getId());
                log.info("Hotels of  was successfully deleted");
            }
        }

        foundCountry.setDeletedAt(Instant.now());
        countryRepository.save(foundCountry);

        log.info("Users was successfully deleted");
    }
}
