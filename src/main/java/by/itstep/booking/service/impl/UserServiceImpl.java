package by.itstep.booking.service.impl;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValueIsTakenException;
import by.itstep.booking.exception.WrongUserPasswordException;
import by.itstep.booking.mapper.UserMapper;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.service.UserService;
import by.itstep.booking.service.security.SecurityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final SecurityService securityService;
    private  final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreteDto dto) {

        UserEntity userEntity = userMapper.map(dto);
        userEntity.setBalance(0.0);
        userEntity.setRole(UserRole.USER);
        userEntity.setBlocked(false);
        userEntity.setPassword(passwordEncoder.encode(dto.getPassword() + dto.getEmail()));

        Optional<UserEntity> entityWithSameEmail = userRepository
                .findByEmail(userEntity.getEmail());

        if(entityWithSameEmail.isPresent()) {
            throw new UniqueValueIsTakenException("UserServiceIml -> Email is taken");
        }

        UserEntity saveEntity = userRepository.save(userEntity);

        log.info("User was successfully created");

        return userMapper.map(saveEntity);
    }

    @Override
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserServiceIml -> UserEntity was not found by id: " + dto.getId())
                );

        securityService.throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());
        userToUpdate.setPhoneNumber(dto.getPhoneNumber());


        UserEntity updatedUser= userRepository.save(userToUpdate);
        log.info("User was successfully updated");

        return userMapper.map(updatedUser);
    }

    @Override
    public UserFullDto findById(int id) {
        UserFullDto foundUser = userRepository
                .findById(id)
                .map(userMapper::map)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "UserServiceIml -> UserEntity was not found by id: " + id)
                );

        log.info("User was successfully found");
        return foundUser;
    }

    @Override
    public Page<UserFullDto> findAll(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        Page<UserEntity> foundEntities = userRepository.findAll(pageable);

        Page<UserFullDto> usersDto = foundEntities.map(userMapper::map);

        log.info("Users was successfully found");log.info("Users was successfully found");

        return usersDto;
    }

    @Override
    public void deleteById(int id) {
        UserEntity foundUser = userRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "UserServiceIml -> UserEntity was not found by id: " + id)
                );

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);
        log.info("User was successfully deleted");
    }

    @Override
    public void changePassword(ChangeUserPasswordDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(()-> new AppEntityNotFoundException("UserServiceIml -> UserEntity was not found by id: "
                        + dto.getUserId()));

        securityService.throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());
        if (passwordEncoder.matches( userToUpdate.getPassword(), dto.getOldPassword() + userToUpdate.getEmail()) ){
            throw new WrongUserPasswordException("Wrong password");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword()+userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        log.info("User password was successfully updated");
    }

    @Override
    public UserFullDto updateBalance(UpdateBalanceDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(()-> new AppEntityNotFoundException(
                        "UserServiceIml -> UserEntity was not found by id: " + dto.getUserId())
                );

        securityService.throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setBalance(userToUpdate.getBalance() + dto.getBalance());

        UserEntity updatedUser = userRepository.save(userToUpdate);

        return userMapper.map(updatedUser);
    }

    @Override
    public UserFullDto changeRole(ChangeUserRoleDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        userToUpdate.setRole(dto.getNewRole());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        log.info("User role was successfully updated");

        return  userMapper.map(updatedUser);
    }

    @Override
    public void blocked(Integer userId) {
        UserEntity entityToUpdate = userRepository
                .findById(userId)
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + userId));

        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        log.info("User was successfully updated.");
    }

}
