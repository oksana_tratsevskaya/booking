package by.itstep.booking.service.impl;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.BusinessLogicException;
import by.itstep.booking.mapper.BookingMapper;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.security.SecurityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;
    private final HotelRepository hotelRepository;
    private final SecurityService securityService;
    private final AuthenticationService authenticationService;

    @Override
    public BookingFullDto create(BookingCreateDto dto) {
        BookingEntity bookingEntity = bookingMapper.map(dto);
        Integer freeRoom = freeRoom(dto);
        bookingEntity.setRoom(roomRepository.getById(freeRoom));

        UserEntity userEntity = userRepository
                .findById(dto.getUserId())
                .orElseThrow(()-> new AppEntityNotFoundException(
                        "BookingServiceImpl -> UserEntity was not found by id: " + dto.getUserId()));

        securityService.throwIfNotCurrentUserOrAdmin(authenticationService.getAuthenticationUser().getEmail());

        payBooking(dto.getHotelId(), dto.getSeatNumber(), userEntity);
        bookingEntity.setUser(userEntity);

        BookingEntity createBooking = bookingRepository.save(bookingEntity);
        log.info("Booking was successfully created " + createBooking);
        BookingFullDto createdFullDto = bookingMapper.map(createBooking);
        log.info("createdDto: " + createdFullDto);

        return createdFullDto;
    }

    @Override
    public BookingFullDto findById(int id) {
        BookingFullDto foundBooking = bookingRepository
                .findById(id)
                .map(bookingMapper::map)
                .orElseThrow(()->new AppEntityNotFoundException(
                        "BookingServiceIml -> BookingEntity was not found by id: " + id)
                );

        log.info("Booking was successfully found");

        return foundBooking;
    }

    @Override
    public List<BookingFullDto> findByUserId(int userId) {
        List<BookingEntity> bookingEntities = bookingRepository
                .findByUserId(userId);

        log.info("Bookings for user with id " + userId + " was successfully found");

        return bookingMapper.map(bookingEntities);
    }

    @Override
    public Page<BookingFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page,size);

        Page<BookingEntity> foundEntities = bookingRepository.findAll(pageable);

        Page<BookingFullDto> bookingFullDtos = foundEntities.map(bookingMapper::map);

        log.info("Booking was successfully found");

        return bookingFullDtos;
    }

    @Override
    public void deleteById(int id) {
        BookingEntity foundBooking = bookingRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "BookingServiceIml -> Booking was not found by id: " + id)
                );

        securityService.throwIfNotCurrentUserOrAdmin(authenticationService.getAuthenticationUser().getEmail());

        bookingRepository.deleteById(id);
        returnMoney(foundBooking);
        log.info("Booking was successfully deleted");
    }

    private Integer freeRoom(BookingCreateDto dto) {
        List<Integer> roomNumbers = roomRepository.findFreeRoom(
                dto.getHotelId(),
                dto.getSeatNumber(),
                dto.getCheckOutDate(),
                dto.getCheckInDate()
        );
        log.info("RoomEntities: " + roomNumbers);
        if (roomNumbers.isEmpty()) {
            throw new AppEntityNotFoundException("BookingServiceImpl -> Not free room");
        }

        List<Integer> freeRooms = new ArrayList<>();
        for (Integer integer : roomNumbers) {
            List<Integer> bookingWithRoom = bookingRepository.findRoomsWithBooking(
                    integer,
                    dto.getCheckOutDate(),
                    dto.getCheckInDate());
            System.out.println("bookingWithRoom" + bookingWithRoom);
            if(bookingWithRoom.isEmpty()){
                freeRooms.add(integer);
            }
        }
        System.out.println(freeRooms);
        if (freeRooms.isEmpty()) {
            throw new AppEntityNotFoundException("BookingServiceImpl -> Not free room");
        }

        return freeRooms.get(0);
    }

    private void payBooking (Integer hotelId, Integer seatNumber, UserEntity userEntity) {

        HotelEntity hotelEntity = hotelRepository
                .findById(hotelId)
                .orElseThrow(()-> new AppEntityNotFoundException(
                        "BookingServiceImpl -> HotelEntity was not found by id: " + hotelId)
                );

        double costForRoom = HotelServiceImpl.hotelCostCalculation(hotelEntity.getPrice(), seatNumber);

        if (userEntity.getBalance() < costForRoom) {
            throw new BusinessLogicException("Not enough money on account");
        }

        userEntity.setBalance(userEntity.getBalance() - costForRoom);
        userRepository.save(userEntity);
        log.info("Payment has been deducted from the balance");
    }

    private void returnMoney(BookingEntity foundBooking) {
        if(foundBooking.getCheckInDate().before(new Date()) || foundBooking.getCheckInDate().equals(new Date())) {
            log.info("The deposit paid is refundable if canceled no earlier than one day in advance. " +
                    "Please contact the administrator");
        } else {
            RoomEntity room = roomRepository
                    .findById(foundBooking.getRoom().getId())
                    .orElseThrow(()-> new AppEntityNotFoundException("Not room with "
                            + foundBooking.getRoom().getId())
                    );
            HotelEntity hotel = room.getHotel();

            UserEntity userEntity = userRepository.getById(foundBooking.getUser().getId());

            userEntity.setBalance(userEntity.getBalance() + HotelServiceImpl.hotelCostCalculation(
                    hotel.getPrice(),
                    room.getSeatNumber())
            );

            userRepository.save(userEntity);
            log.info("Prepayment returned to balance");
        }
    }
}
