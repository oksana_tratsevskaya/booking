package by.itstep.booking.service.impl;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.select.AvailableHotelsRequest;
import by.itstep.booking.dto.select.AvailableHotelsResponse;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.mapper.HotelMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.service.HotelService;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelMapper hotelMapper;
    private final HotelRepository hotelRepository;
    private final CountryRepository countryRepository;
    private final RoomService roomService;

    @Override
    @Transactional
    public HotelFullDto create(HotelCreateDto dto) {
        HotelEntity entityToCreate = hotelMapper.map(dto);
        entityToCreate.setCountry(
                countryRepository
                        .findById(dto.getCountryId())
                        .orElseThrow(() -> new AppEntityNotFoundException(
                                "CountryEntity was not found"))
        );
        HotelEntity hotelCreated = hotelRepository.save(entityToCreate);

        log.info("Hotel was added in BD");

        return hotelMapper.map(hotelCreated);
    }

    @Override
    @Transactional
    public HotelFullDto update(HotelUpdateDto dto) {
        HotelEntity foundHotel = hotelRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "HotelEntity was not found by id: " + dto.getId())
                );

        foundHotel.setName(dto.getName());
        foundHotel.setAddress(dto.getAddress());
        foundHotel.setRating(dto.getRating());

        log.info("Hotel with id: " + foundHotel.getId() + " was updated");

        return hotelMapper.map(foundHotel);
    }

    @Override
    @Transactional
    public HotelFullDto findById(int id) {
        HotelFullDto foundDto = hotelRepository
                .findById(id)
                .map(hotelMapper::map)
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "HotelEntity was not found by id: " + id)
                );
        log.info("Hotel with id: " + foundDto.getId() + " was found");

        return foundDto;
    }

    @Override
    @Transactional
    public Page<HotelFullDto> findAll(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        Page<HotelEntity> foundEntities = hotelRepository.findAll(pageable);

        Page<HotelFullDto> usersDto= foundEntities.map(entity -> hotelMapper.map(entity));

        log.info("Users was successfully found");

        return usersDto;
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        HotelEntity foundHotel = hotelRepository
                .findById(id)
                .orElseThrow(()-> new AppEntityNotFoundException(
                        "HotelEntity was not found by id: " + id)
                );

        List<RoomEntity> hotelRooms = foundHotel.getRooms();
        if (hotelRooms == null) {
            log.info("Not hotel's rooms");
        } else {
            for (RoomEntity entity : hotelRooms) {
                roomService.deleteById(entity.getId());
                log.info("Rooms of  was successfully deleted");
            }
        }
        foundHotel.setDeletedAt(Instant.now());
        hotelRepository.save(foundHotel);

        log.info("Hotel with id" + id + " was successfully deleted");
    }

    @Override
    public List <AvailableHotelsResponse> findAvailableHotels(AvailableHotelsRequest dto) {
        CountryEntity countryEntity = countryRepository.findById(dto.getCountryId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "HotelServiceImpl -> CountryEntity was not found by id: " + dto.getCountryId()));
        if (countryEntity.getHotels().isEmpty()){
            throw new AppEntityNotFoundException(
                    "HotelServiceImpl -> No hotels found for country with id: " + dto.getCountryId());
        }

        List<Integer> hotelsId = hotelRepository.findAvailableHotels(
                dto.getCountryId(),
                dto.getSeatNumber(),
                dto.getCheckOutDate(),
                dto.getCheckInDate()
        );
        log.info("hotelId: " + hotelsId);
        if (hotelsId.isEmpty()) {
            throw new AppEntityNotFoundException("HotelServiceImpl -> Not available hotels");
        }

        List<HotelEntity> availableHotels = new ArrayList<>();
        for (Integer hotelId : hotelsId) {
            List<Integer> bookingWithRoom = hotelRepository.findBookingWithSameDate(
                    hotelId,
                    dto.getCheckOutDate(),
                    dto.getCheckInDate());
            log.info("bookingWithRoom" + bookingWithRoom);
            if(bookingWithRoom.isEmpty()){
                availableHotels.add(hotelRepository.getById(hotelId));
            }
        }
        if (availableHotels.isEmpty()) {
            throw new AppEntityNotFoundException("HotelServiceImpl -> Not available hotels");
        }
        System.out.println(availableHotels);

        return hotelMapper.mapAvailableHotels(availableHotels);
    }

    public static Double hotelCostCalculation (Double hotelCost, Integer seatNumber) {
        Double roomCost = 0.0;
        switch (seatNumber) {
            case 1:
                roomCost = hotelCost;
                break;
            case 2:
                roomCost = hotelCost * 1.25;
                break;
            case 3:
                roomCost = hotelCost * 1.5;
                break;
            case 4:
                roomCost = hotelCost * 1.75;
                break;
            case 5:
                roomCost = hotelCost * 2;
                break;
        };

        return roomCost;
    }
}
