package by.itstep.booking.service;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface RoomService {

    RoomFullDto create (RoomCreateDto dto);

    RoomFullDto update (RoomUpdateDto dto);

    RoomFullDto findById (int id);

    Page<RoomFullDto> findAll(int page, int size);

    void deleteById(int id);

}
