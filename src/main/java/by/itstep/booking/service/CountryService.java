package by.itstep.booking.service;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CountryService {

    CountryDto create(CountryCreateDto dto);

    CountryDto update(CountryUpdateDto dto);

    CountryDto findById(int id);

    Page<CountryDto> findAll(int page, int size);

    void deleteById(int id);

}
