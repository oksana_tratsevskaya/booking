package by.itstep.booking.service;


import by.itstep.booking.dto.user.*;
import org.springframework.data.domain.Page;

public interface UserService {

    UserFullDto create(UserCreteDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    Page<UserFullDto> findAll(int page, int size);

    void deleteById(int id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void blocked(Integer userId);

    UserFullDto updateBalance(UpdateBalanceDto dto);
}
