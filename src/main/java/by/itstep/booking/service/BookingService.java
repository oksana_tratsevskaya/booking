package by.itstep.booking.service;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BookingService {

    BookingFullDto create (BookingCreateDto dto);

    BookingFullDto findById (int id);

    List<BookingFullDto> findByUserId(int userId);

    Page<BookingFullDto> findAll(int page, int size);

    void deleteById(int id);



}
