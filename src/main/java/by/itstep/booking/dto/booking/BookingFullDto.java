package by.itstep.booking.dto.booking;

import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.user.UserFullDto;
import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class BookingFullDto {

    private Integer id;
    private RoomShortDto room;
    private Date checkInDate;
    private Date checkOutDate;
    private UserFullDto user;

}
