package by.itstep.booking.dto.booking;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class BookingCreateDto {

    @NotNull
    private Integer hotelId;

    @NotNull
    private Integer seatNumber;

    @NotNull
    private Integer userId;

    @FutureOrPresent
    private Date checkInDate;

    @Future
    private Date checkOutDate;
}
