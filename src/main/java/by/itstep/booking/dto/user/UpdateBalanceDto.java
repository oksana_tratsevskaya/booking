package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Data
public class UpdateBalanceDto {

    @DecimalMin("0.01")
    @DecimalMax("1000000.0")
    private double balance;

    @NotNull
    private Integer userId;

}
