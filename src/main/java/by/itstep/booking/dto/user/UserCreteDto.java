package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UserCreteDto {

    @NotNull
    @Size(min = 1, max = 20)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 60)
    private String lastName;

    @NotNull
    @Size(min = 7, max = 14)
    private String phoneNumber;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Pattern(message = "The password must contain a number, a lowercase latin character,\n" +
            " capital latin character, special character such as ! @ # & ( ).\n" +
            "The password must contain at least 8 characters and no more than 20 characters.",
            regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()â€\"[{}]:;',?/*~$^+=<>]).{5,20}$")
    private String password;



}
