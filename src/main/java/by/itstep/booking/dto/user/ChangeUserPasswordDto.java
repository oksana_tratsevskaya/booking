package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ChangeUserPasswordDto {

    @NotNull
    private Integer userId;

    @NotNull
    private String oldPassword;

    @Pattern(message = "The password must contain a number, a lowercase latin character,\n" +
            " capital latin character, special character such as ! @ # & ( ).\n" +
            "The password must contain at least 8 characters and no more than 20 characters.",
            regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()â€\"[{}]:;',?/*~$^+=<>]).{5,20}$")
    private String newPassword;

}
