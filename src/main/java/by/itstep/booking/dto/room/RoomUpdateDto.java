package by.itstep.booking.dto.room;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class RoomUpdateDto {

    @NotNull
    private Integer id;

    @Min(value = 1)
    @Max(value = 10)
    private Integer seatNumber;

}
