package by.itstep.booking.dto.room;

import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class RoomFullDto {

    private Integer id;
    private Integer number;
    private Integer seatNumber;
    private HotelShortDto hotel;
}
