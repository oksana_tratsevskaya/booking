package by.itstep.booking.dto.room;

import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoomShortDto {

    private Integer id;
    private Integer number;
    private HotelShortDto hotel;
}
