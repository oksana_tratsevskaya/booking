package by.itstep.booking.dto.room;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class RoomCreateDto {

    @NotNull
    private Integer number;

    @Min(value = 1)
    @Max(value = 10)
    private Integer seatNumber;

    @NotNull
    private Integer hotelId;

}
