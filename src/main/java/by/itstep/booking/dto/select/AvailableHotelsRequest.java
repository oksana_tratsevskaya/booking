package by.itstep.booking.dto.select;

import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class AvailableHotelsRequest {

    @NotBlank
    private Integer countryId;

    @NotNull
    private Integer seatNumber;

    @FutureOrPresent
    private Date checkInDate;

    @Future
    private Date checkOutDate;

}
