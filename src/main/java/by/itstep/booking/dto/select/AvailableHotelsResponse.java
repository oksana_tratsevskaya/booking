package by.itstep.booking.dto.select;

import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

@Data
public class AvailableHotelsResponse {

    private String name;
    private Double price;
}
