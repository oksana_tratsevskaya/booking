package by.itstep.booking.dto.country;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
public class CountryCreateDto {

    @NotBlank
    private String name;

}
