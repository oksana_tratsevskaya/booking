package by.itstep.booking.dto.country;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CountryUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

}
