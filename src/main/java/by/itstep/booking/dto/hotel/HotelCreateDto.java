package by.itstep.booking.dto.hotel;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class HotelCreateDto {

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @NotBlank
    @Size(min = 1, max = 256)
    private String address;

    @NotNull
    private Double price;

    @DecimalMin(value = "0.1")
    @DecimalMax(value = "10.0")
    private Double rating;

    @Min(value = 5)
    @Max(value = 1)
    private Integer starRating;

    @NotBlank
    private Integer countryId;

}
