package by.itstep.booking.dto.hotel;

import by.itstep.booking.dto.country.CountryDto;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class HotelFullDto {

    private Integer id;
    private String name;
    private Double price;
    private String address;
    private Double rating;
    private Integer starRating;
    private CountryDto country;

}
