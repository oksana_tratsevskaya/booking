package by.itstep.booking.dto.hotel;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class HotelUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @NotBlank
    @Size(min = 1, max = 256)
    private String address;

    @DecimalMin(value = "0.1")
    @DecimalMax(value = "10")
    private Double rating;

}
