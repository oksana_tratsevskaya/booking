package by.itstep.booking.repository;

import by.itstep.booking.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface HotelRepository extends JpaRepository<HotelEntity, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT hotels.id " +
                    "FROM booking.hotels " +
                    "LEFT JOIN booking.rooms ON hotels.id = rooms.hotel_id " +
                    "LEFT JOIN booking.bookings ON rooms.id = bookings.room_id " +
                    "WHERE hotels.country_id = :countryId " +
                    "AND rooms.seat_number = :seatNumber " +
                    "AND ((NOT((bookings.check_in_date >= :checkInDate " +
                    "AND bookings.check_out_date <= :checkOutDate))) " +
                    "OR room_id IS NULL);")
    List<Integer> findAvailableHotels(
            @Param("countryId") Integer countryId,
            @Param("seatNumber") Integer seatNumber,
            @Param("checkOutDate") Date checkOutDate,
            @Param("checkInDate") Date checkInDate
    );

    @Query(nativeQuery = true,
            value = "SELECT hotels.id " +
                    "FROM booking.hotels " +
                    "LEFT JOIN booking.rooms ON hotels.id = rooms.hotel_id " +
                    "LEFT JOIN booking.bookings ON rooms.id = bookings.room_id " +
                    "WHERE (bookings.check_in_date >= :checkInDate " +
                    "AND bookings.check_out_date <= :checkOutDate) " +
                    "AND hotels.id  = :hotelId ;")
    List<Integer> findBookingWithSameDate(
            @Param("hotelId") Integer hotelId,
            @Param("checkOutDate") Date checkOutDate,
            @Param("checkInDate") Date checkInDate
    );
}
