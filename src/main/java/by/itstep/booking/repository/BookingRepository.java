package by.itstep.booking.repository;

import by.itstep.booking.entity.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {

    List<BookingEntity> findByUserId(Integer userId);

    @Query(nativeQuery = true, value = "SELECT id FROM booking.bookings " +
            "WHERE ((bookings.check_in_date >= :checkInDate " +
            "AND bookings.check_out_date <= :checkOutDate)) AND room_id = :roomId ;")
    List<Integer> findRoomsWithBooking(
            @Param("roomId") Integer roomId,
            @Param("checkOutDate") Date checkOutDate,
            @Param("checkInDate") Date checkInDate
    );

}
