package by.itstep.booking.repository;

import by.itstep.booking.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {


    @Query(nativeQuery = true,
            value = "SELECT rooms.id " +
                    "FROM booking.rooms " +
                    "LEFT JOIN bookings " +
                    "ON rooms.id LIKE bookings.room_id " +
                    "WHERE rooms.hotel_id = :hotelId " +
                    "AND rooms.seat_number = :seaNumber " +
                    "AND ((NOT((bookings.check_in_date >= :checkInDate AND bookings.check_out_date <= :checkOutDate))) " +
                    "OR room_id IS NULL)" +
                    "GROUP BY rooms.id;")
    List<Integer> findFreeRoom(
            @Param("hotelId") Integer hotelId,
            @Param("seaNumber") Integer seaNumber,
            @Param("checkOutDate") Date checkOutDate,
            @Param("checkInDate") Date checkInDate
    );
}
