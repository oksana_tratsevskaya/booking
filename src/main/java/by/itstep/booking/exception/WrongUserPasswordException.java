package by.itstep.booking.exception;

public class WrongUserPasswordException extends RuntimeException{

    public WrongUserPasswordException(String message) {
        super(message);
    }
}
