package by.itstep.booking.exception;

public class UniqueValueIsTakenException extends RuntimeException{

    public  UniqueValueIsTakenException(String message) {
        super(message);
    }

}
