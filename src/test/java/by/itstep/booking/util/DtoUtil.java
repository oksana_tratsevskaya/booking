package by.itstep.booking.util;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.dto.select.AvailableHotelsRequest;
import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.UserRole;

import java.sql.Date;

import static by.itstep.booking.BookingApplicationTests.FAKER;

public class DtoUtil {

    public static CountryCreateDto createCountryCreateDto() {
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName(FAKER.address().country());

        return createDto;
    }

    public static CountryUpdateDto createCountryUpdateDto(int existEntityId) {
        CountryUpdateDto updateDto = new CountryUpdateDto();
        updateDto.setId(existEntityId);
        updateDto.setName(FAKER.address().country());
        System.out.println("createCountryUpdateDto ----------->" + updateDto);

        return updateDto;
    }

    public static HotelCreateDto createHotelCreateDto(int countryId) {
        HotelCreateDto createDto = new HotelCreateDto();
        double scale = Math.pow(10, 2);
        createDto.setName(FAKER.cat().name());
        createDto.setAddress(FAKER.address().cityName() + " " + FAKER.address().streetAddress());
        createDto.setPrice(Math.ceil(5+(Math.random()*955)*scale)/scale);
        createDto.setRating(Math.ceil((Math.random()*10)*scale)/scale);
        createDto.setStarRating(1+(int)(Math.random()*5));
        createDto.setCountryId(countryId);
        System.out.println("createHotelCreateDto--------------->>>" +createDto);
        return createDto;
    }

    public static HotelUpdateDto createHotelUpdateDto(int id) {
        HotelUpdateDto updateDto = new HotelUpdateDto();
        updateDto.setId(id);
        updateDto.setName(FAKER.book().title());
        updateDto.setAddress(FAKER.address().cityName() + " " + FAKER.address().streetAddress());
        updateDto.setRating(8.0);
        System.out.println("createHotelUpdateDto--------------->>>" +updateDto);
        return updateDto;
    }

    public static RoomCreateDto createRoomCreateDto(int hotelId) {
        RoomCreateDto roomCreateDto = new RoomCreateDto();
        roomCreateDto.setHotelId(hotelId);
        roomCreateDto.setSeatNumber(1+(int)(Math.random()*5));
        roomCreateDto.setNumber(1+(int)(Math.random()*1000));
        return roomCreateDto;
    }

    public static RoomUpdateDto createRoomUpdateDto(int hotelId) {
        RoomUpdateDto roomUpdateDto = new RoomUpdateDto();
        roomUpdateDto.setSeatNumber(1+(int)(Math.random()*5));
        roomUpdateDto.setId(hotelId);
        return roomUpdateDto;
    }

    public static UserCreteDto creteUserCreateDto() {
        UserCreteDto createdUserCreateDTO = new UserCreteDto();
        createdUserCreateDTO.setEmail(FAKER.internet().emailAddress());
        createdUserCreateDTO.setFirstName(FAKER.name().firstName());
        createdUserCreateDTO.setLastName(FAKER.name().lastName());
        createdUserCreateDTO.setPassword(FAKER.internet().password());
        createdUserCreateDTO.setPhoneNumber(FAKER.phoneNumber().phoneNumber());

        System.out.println("createdUserCreateDTO " + createdUserCreateDTO);
        return createdUserCreateDTO;
    }

    public static UserUpdateDto createUserUpdateDto(int id) {
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setId(id);
        updateDto.setFirstName(FAKER.name().firstName());
        updateDto.setLastName(FAKER.name().lastName());
        updateDto.setPhoneNumber(FAKER.phoneNumber().phoneNumber());

        return updateDto;
    }

    public static UpdateBalanceDto createUpdateBalanceDto(int userId) {
        UpdateBalanceDto dto = new UpdateBalanceDto();
        double scale = Math.pow(10, 2);
        dto.setUserId(userId);
        dto.setBalance(Math.ceil((Math.random()*1000000)*scale)/scale);

        return dto;
    }

    public static ChangeUserPasswordDto createChangeUserPasswordDto(Integer userId, String oldPassword) {
        ChangeUserPasswordDto dto = new ChangeUserPasswordDto();
        dto.setUserId(userId);
        dto.setNewPassword(FAKER.internet().password());
        dto.setOldPassword(oldPassword);

        return dto;
    }

    public static ChangeUserRoleDto createChangeUserRoleDto(Integer userId, UserRole userRole) {
        ChangeUserRoleDto dto = new ChangeUserRoleDto();
        dto.setUserId(userId);
        dto.setNewRole(userRole);

        return  dto;
    }

    public static BookingCreateDto createBookingDto(int hotelId, int userId) {
        BookingCreateDto dto = new BookingCreateDto();
        dto.setCheckOutDate(new Date(2022, 05, 01));
        dto.setCheckInDate(new Date(2022, 05, 01));
        dto.setHotelId(hotelId);
        dto.setUserId(userId);
        dto.setSeatNumber(2);

        return dto;
    }

    public static AvailableHotelsRequest createAvailableHotelsRequest(CountryEntity countryEntity) {
        AvailableHotelsRequest availableHotelsRequest = new AvailableHotelsRequest();
        availableHotelsRequest.setCheckInDate(new Date(2022, 06, 02));
        availableHotelsRequest.setCheckOutDate(new Date(2022, 06, 03));
        availableHotelsRequest.setCountryId(countryEntity.getId());
        availableHotelsRequest.setSeatNumber(2);


        return availableHotelsRequest;
    }
}
