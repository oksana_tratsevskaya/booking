package by.itstep.booking.util;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtHelper {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private EntityUtil entityUtil;

    public String createToken(String email) {
        return "Bearer " + jwtTokenProvider.createToken(email);
    }

    public String takeJwtToken(UserRole userRole) {
        UserEntity currentUser = entityUtil.addUserEntityToDb(userRole);

        return createToken(currentUser.getEmail());
    }
}
