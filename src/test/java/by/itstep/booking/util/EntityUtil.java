package by.itstep.booking.util;

import by.itstep.booking.entity.*;
import by.itstep.booking.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;

import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class EntityUtil {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;



    public CountryEntity addCountryEntityToDb() {
        CountryEntity countryEntity = new CountryEntity();
        countryEntity.setName(FAKER.address().country());
        return countryRepository.save(countryEntity);
    }

    public HotelEntity addHotelEntityToDb(CountryEntity countryEntity) {
        HotelEntity hotelEntity = new HotelEntity();
        double scale = Math.pow(10, 2);
        hotelEntity.setCountry(countryEntity);
        hotelEntity.setName(FAKER.cat().name());
        hotelEntity.setAddress(FAKER.address().cityName() + " " + FAKER.address().streetAddress());
        hotelEntity.setPrice(Math.ceil(5+(Math.random()*955)*scale)/scale);
        hotelEntity.setRating(Math.ceil((Math.random()*10)*scale)/scale);
        hotelEntity.setStarRating(1+(int)(Math.random()*5));

        countryEntity.getHotels().add(hotelEntity);

        return hotelRepository.save(hotelEntity);
    }

    public RoomEntity addRoomEntityToDb(HotelEntity hotelEntity) {
        RoomEntity roomEntity = new RoomEntity();
        roomEntity.setHotel(hotelEntity);
        roomEntity.setSeatNumber(2);
        roomEntity.setNumber(1+(int)(Math.random()*1000));

        hotelEntity.getRooms().add(roomEntity);

        return roomRepository.save(roomEntity);
    }

    public UserEntity addUserEntityToDb(UserRole userRole) {
        UserEntity userEntity = new UserEntity();
        String email = FAKER.internet().emailAddress();
        userEntity.setBalance(10000.0);
        userEntity.setPhoneNumber(FAKER.phoneNumber().phoneNumber());
        userEntity.setLastName(FAKER.name().lastName());
        userEntity.setEmail(email);
        userEntity.setFirstName(FAKER.name().firstName());
        userEntity.setRole(userRole);
        userEntity.setPassword(passwordEncoder.encode(FAKER.internet().password() + email));
        userEntity.setBlocked(false);

        return userRepository.save(userEntity);
    }

    public BookingEntity addBookingEntityToDb(UserEntity userEntity, RoomEntity roomEntity) {
        BookingEntity bookingEntity = new BookingEntity();
        bookingEntity.setUser(userEntity);
        bookingEntity.setRoom(roomEntity);
        bookingEntity.setSeatNumber(2);
        bookingEntity.setCheckInDate(new Date(2022, 05, 01));
        bookingEntity.setCheckOutDate(new Date(2022, 05, 05));
        return bookingRepository.save(bookingEntity);
    }

    public HotelEntity addHotelToDbWithAddictedEntity() {
        CountryEntity countryEntity = addCountryEntityToDb();

        return addHotelEntityToDb(countryEntity);
    }

    public RoomEntity addRoomToDbWithAddictedEntity() {
        CountryEntity countryEntity = addCountryEntityToDb();
        HotelEntity hotelEntity = addHotelEntityToDb(countryEntity);

        return roomRepository.save(addRoomEntityToDb(hotelEntity));
    }

    public BookingEntity addBookingToDbWithAddictedEntity(UserEntity userEntity) {
        CountryEntity countryEntity = addCountryEntityToDb();
        HotelEntity hotelEntity = addHotelEntityToDb(countryEntity);
        RoomEntity roomEntity = addRoomEntityToDb(hotelEntity);

        return bookingRepository.save(addBookingEntityToDb(userEntity, roomEntity));
    }



}
