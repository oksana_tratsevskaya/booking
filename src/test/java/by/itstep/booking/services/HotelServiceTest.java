package by.itstep.booking.services;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.HotelService;
import by.itstep.booking.service.RoomService;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class HotelServiceTest extends BookingApplicationTests {

    @Autowired
    private HotelService hotelService;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserRepository userRepository;
    @MockBean
    AuthenticationService authenticationService;
    @Autowired
    private EntityUtil entityUtil;

    @Test
    @Transactional
    public void create_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryEntity countryEntityToDB =entityUtil.addCountryEntityToDb();
        HotelCreateDto hotelCreateDto = DtoUtil.createHotelCreateDto(countryEntityToDB.getId());

        //when
        HotelFullDto createdDto = hotelService.create(hotelCreateDto);

        //then
        Assertions.assertNotNull(createdDto.getId());
        HotelEntity createdEntity = hotelRepository.getById(createdDto.getId());
        Assertions.assertEquals(createdEntity.getName(), hotelCreateDto.getName());
        Assertions.assertEquals(createdEntity.getAddress(), hotelCreateDto.getAddress());
        Assertions.assertEquals(createdEntity.getCountry().getId(), hotelCreateDto.getCountryId());
        Assertions.assertEquals(createdEntity.getPrice(), hotelCreateDto.getPrice());
        Assertions.assertEquals(createdEntity.getRating(), hotelCreateDto.getRating());
        Assertions.assertEquals(createdEntity.getStarRating(), hotelCreateDto.getStarRating());
    }

    @Test
    @Transactional
    public void update_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        HotelEntity createdDto = entityUtil.addHotelToDbWithAddictedEntity();
        HotelUpdateDto hotelUpdateDto = DtoUtil.createHotelUpdateDto(createdDto.getId());

        //when
        HotelFullDto dtoAfterUpdated = hotelService.update(hotelUpdateDto);

        //then
        Assertions.assertEquals(dtoAfterUpdated.getId(),createdDto.getId());
        HotelEntity updateEntity = hotelRepository.getById(dtoAfterUpdated.getId());
        Assertions.assertEquals(updateEntity.getName(), hotelUpdateDto.getName());
        Assertions.assertEquals(updateEntity.getAddress(), hotelUpdateDto.getAddress());
        Assertions.assertEquals(updateEntity.getRating(), hotelUpdateDto.getRating());

        Assertions.assertEquals(updateEntity.getPrice(), createdDto.getPrice());
        Assertions.assertEquals(updateEntity.getStarRating(), createdDto.getStarRating());
    }

    @Test
    @Transactional
    public void findById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        HotelEntity createdDto = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        HotelFullDto foundedDto = hotelService.findById(createdDto.getId());

        //then
        Assertions.assertEquals(foundedDto.getId(),createdDto.getId());
        Assertions.assertEquals(foundedDto.getName(), createdDto.getName());
        Assertions.assertEquals(foundedDto.getAddress(), createdDto.getAddress());
        Assertions.assertEquals(foundedDto.getRating(), createdDto.getRating());
        Assertions.assertEquals(foundedDto.getPrice(), createdDto.getPrice());
        Assertions.assertEquals(foundedDto.getStarRating(), createdDto.getStarRating());
    }

    @Test
    @Transactional
    public void findAll_pappyPath() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        entityUtil.addHotelToDbWithAddictedEntity();
        entityUtil.addHotelToDbWithAddictedEntity();
        entityUtil.addHotelToDbWithAddictedEntity();

        //when
        Page<HotelFullDto> foundDtos = hotelService.findAll(1, 3);

        //then
        Assertions.assertEquals(3, foundDtos.getTotalElements());
    }

    @Test
    @Transactional
    public void deleteById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        HotelEntity createdDto = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        hotelService.deleteById(createdDto.getId());

        //then
        HotelEntity deletedEntity = hotelRepository.getById(createdDto.getId());
        Assertions.assertNotNull(deletedEntity.getDeletedAt());

    }

    @Test
    @Transactional
    public void deleteById_happyPass_when_several_rooms() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        HotelEntity createdHotel = entityUtil.addHotelToDbWithAddictedEntity();
        RoomEntity room1 = entityUtil.addRoomEntityToDb(createdHotel);
        RoomEntity room2 = entityUtil.addRoomEntityToDb(createdHotel);
        RoomEntity room3 = entityUtil.addRoomEntityToDb(createdHotel);

        //when
        hotelService.deleteById(createdHotel.getId());

        //then
        HotelEntity deletedEntity = hotelRepository.getById(createdHotel.getId());
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
        RoomEntity deletedRoom1 = roomRepository.getById(room1.getId());
        Assertions.assertNotNull(deletedRoom1.getDeletedAt());
        RoomEntity deletedRoom2 = roomRepository.getById(room2.getId());
        Assertions.assertNotNull(deletedRoom2.getDeletedAt());
        RoomEntity deletedRoom3 = roomRepository.getById(room3.getId());
        Assertions.assertNotNull(deletedRoom3.getDeletedAt());
    }
}
