package by.itstep.booking.services;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.RoomService;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class RoomServiceTest extends BookingApplicationTests {

    @Autowired
    CountryRepository countryRepository;
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    RoomService roomService;
    @Autowired
    UserRepository userRepository;
    @MockBean
    AuthenticationService authenticationService;
    @Autowired
    private EntityUtil entityUtil;

    @Test
    @Transactional
    public void create_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        HotelEntity createdHotel = entityUtil.addHotelToDbWithAddictedEntity();
        RoomCreateDto roomCreateDto = DtoUtil.createRoomCreateDto(createdHotel.getId());

        //when
        RoomFullDto createdDto = roomService.create(roomCreateDto);

        //then
        Assertions.assertNotNull(createdDto.getId());
        RoomEntity createdEntity = roomRepository.getById(createdDto.getId());
        Assertions.assertEquals(createdEntity.getHotel().getId(), roomCreateDto.getHotelId());
        Assertions.assertEquals(createdEntity.getNumber(), roomCreateDto.getNumber());
        Assertions.assertEquals(createdEntity.getSeatNumber(), roomCreateDto.getSeatNumber());
    }

    @Test
    @Transactional
    public void update_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        RoomEntity roomInDb = entityUtil.addRoomToDbWithAddictedEntity();
        RoomUpdateDto roomUpdateDto = DtoUtil.createRoomUpdateDto(roomInDb.getId());

        //when
        RoomFullDto dtoAfterUpdated = roomService.update(roomUpdateDto);

        //then
        Assertions.assertEquals(dtoAfterUpdated.getId(),roomInDb.getId());
        RoomEntity updatedEntity = roomRepository.getById(dtoAfterUpdated.getId());
        Assertions.assertEquals(updatedEntity.getSeatNumber(), roomUpdateDto.getSeatNumber());

        Assertions.assertEquals(updatedEntity.getHotel().getId(), roomInDb.getHotel().getId());
        Assertions.assertEquals(updatedEntity.getNumber(), roomInDb.getNumber());
    }

    @Test
    @Transactional
    public void findById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        RoomEntity roomInDb = entityUtil.addRoomToDbWithAddictedEntity();

        //when
        RoomFullDto foundedDto = roomService.findById(roomInDb.getId());

        //then
        Assertions.assertEquals(foundedDto.getId(), roomInDb.getId());
        Assertions.assertEquals(foundedDto.getHotel().getId(),roomInDb.getHotel().getId());
        Assertions.assertEquals(foundedDto.getNumber(),roomInDb.getNumber());
        Assertions.assertEquals(foundedDto.getSeatNumber(),roomInDb.getSeatNumber());
    }

    @Test
    @Transactional
    public void findAll_pappyPath() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        entityUtil.addRoomToDbWithAddictedEntity();
        entityUtil.addRoomToDbWithAddictedEntity();
        entityUtil.addRoomToDbWithAddictedEntity();

        //when
        Page<RoomFullDto> foundRooms = roomService.findAll(1, 3);

        //then
        Assertions.assertEquals(3, foundRooms.getTotalElements());
    }

    @Test
    @Transactional
    public void deleteById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        RoomEntity roomInDb = entityUtil.addRoomToDbWithAddictedEntity();

        //when
        roomService.deleteById(roomInDb.getId());

        //then
        RoomEntity deletedEntity = roomRepository.getById(roomInDb.getId());
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
    }
}
