package by.itstep.booking.services;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.*;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.UserService;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class BookingServiceTest extends BookingApplicationTests {

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntityUtil entityUtil;

    @MockBean
    AuthenticationService authenticationService;


    @Test
    @Transactional
    public void create_happyPass() {
        //given
        UserEntity createdUser = entityUtil.addUserEntityToDb(UserRole.USER);
        HotelEntity createdHotel = entityUtil.addHotelToDbWithAddictedEntity();
        RoomEntity createdRoom = entityUtil.addRoomEntityToDb(createdHotel);

        BookingCreateDto bookingCreateDto = DtoUtil.createBookingDto(
                createdHotel.getId(),
                createdUser.getId()
        );
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(createdUser);

        //when
        BookingFullDto dto = bookingService.create(bookingCreateDto);

        //then
        Assertions.assertNotNull(dto.getId());
        BookingEntity bookingEntity = bookingRepository.getById(dto.getId());
        Assertions.assertEquals(bookingEntity.getRoom().getId(), createdRoom.getId());
        Assertions.assertEquals(bookingEntity.getCheckInDate(), bookingCreateDto.getCheckInDate());
        Assertions.assertEquals(bookingEntity.getCheckOutDate(), bookingCreateDto.getCheckOutDate());
        Assertions.assertEquals(bookingEntity.getUser().getId(), bookingCreateDto.getUserId());

    }

    @Test
    @Transactional
    public void findById_happyPass() {
        //given
        UserEntity createdUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(createdUser);
        BookingEntity dto = entityUtil.addBookingToDbWithAddictedEntity(createdUser);

        //when
        BookingFullDto foundDto = bookingService.findById(dto.getId());

        //then
        Assertions.assertEquals(foundDto.getRoom().getId(), dto.getRoom().getId());
        Assertions.assertEquals(foundDto.getUser().getId(), dto.getUser().getId());
    }

    @Test
    @Transactional
    public void findByUserId_happyPass() {
        //given
        UserEntity createdUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser);

        //when
        List<BookingFullDto> foundDto = bookingService.findByUserId(createdUser.getId());

        //then
        Assertions.assertEquals(2, foundDto.size());
    }

    @Test
    @Transactional
    public void findAll_pappyPath() {
        //given
        UserEntity createdUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser);
        UserEntity createdUser1 = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser1);
        entityUtil.addBookingToDbWithAddictedEntity(createdUser1);


        //when
        Page<BookingFullDto> foundBookings = bookingService.findAll(1, 4);

        //then
        Assertions.assertEquals(4, foundBookings.getTotalElements());
    }

    @Test
    @Transactional
    public void deleteById_happyPass() {
        //given
        UserEntity createdUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        BookingEntity entity = entityUtil.addBookingToDbWithAddictedEntity(createdUser);;

        //when
        bookingService.deleteById(entity.getId());

        //then
        Assertions.assertNotNull(bookingRepository.getById(entity.getId()).getDeletedAt());
    }


}
