package by.itstep.booking.services;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.user.UpdateBalanceDto;
import by.itstep.booking.dto.user.UserCreteDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserUpdateDto;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.security.JwtTokenProvider;
import by.itstep.booking.service.UserService;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class UserServiceTest extends BookingApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @MockBean
    AuthenticationService authenticationService;
    @Autowired
    private EntityUtil entityUtil;
    

    @Test
    @Transactional
    public void create_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        UserCreteDto createDto = DtoUtil.creteUserCreateDto();

        //when
        UserFullDto dto = userService.create(createDto);

        //then
        Assertions.assertNotNull(dto.getId());
        UserEntity foundUser = userRepository.getById(dto.getId());
        Assertions.assertEquals(foundUser.getEmail(),createDto.getEmail());
        Assertions.assertEquals(foundUser.getFirstName(),createDto.getFirstName());
        Assertions.assertEquals(foundUser.getLastName(), createDto.getLastName());
        Assertions.assertEquals(foundUser.getPhoneNumber(), createDto.getPhoneNumber());
        Assertions.assertNotNull(foundUser.getPassword());
    }

    //Не рпботает нужен фикс
    @Test
    @Transactional
    public void update_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        UserUpdateDto userUpdateDto = DtoUtil.createUserUpdateDto(userEntity.getId());

        //when
        UserFullDto dtoAfterUpdated = userService.update(userUpdateDto);

        //then
        Assertions.assertEquals(userEntity.getId(), dtoAfterUpdated.getId());
        UserEntity updatedUser = userRepository.getById(dtoAfterUpdated.getId());
        Assertions.assertEquals(userEntity.getEmail(),updatedUser.getEmail());
        Assertions.assertEquals(userUpdateDto.getFirstName(),updatedUser.getFirstName());
        Assertions.assertEquals(userUpdateDto.getLastName(), updatedUser.getLastName());
        Assertions.assertEquals(userUpdateDto.getPhoneNumber(), updatedUser.getPhoneNumber());
    }

    @Test
    @Transactional
    public void findById_happyPass() {
        //given
        UserEntity existEntity = entityUtil.addUserEntityToDb(UserRole.USER);

        //when
        UserFullDto foundDto = userService.findById(existEntity.getId());

        //then
        Assertions.assertEquals(existEntity.getEmail(),existEntity.getEmail());
        Assertions.assertEquals(foundDto.getFirstName(),existEntity.getFirstName());
        Assertions.assertEquals(foundDto.getLastName(), existEntity.getLastName());
        Assertions.assertEquals(foundDto.getPhoneNumber(), existEntity.getPhoneNumber());
    }

    @Test
    @Transactional
    public void findAll_pappyPath() {
        //given
        entityUtil.addUserEntityToDb(UserRole.USER);
        entityUtil.addUserEntityToDb(UserRole.USER);
        entityUtil.addUserEntityToDb(UserRole.USER);

        //when
        Page<UserFullDto> foundDto = userService.findAll(1, 3);

        //then
        Assertions.assertEquals(3, foundDto.getTotalElements());

    }

    @Test
    @Transactional
    public void deleteById_happyPass() {
        //given
        UserEntity existEntity = entityUtil.addUserEntityToDb(UserRole.USER);

        //when
        userService.deleteById(existEntity.getId());

        //then
        Assertions.assertNotNull(userRepository.getById(existEntity.getId()).getDeletedAt());
    }


    //Не рпботает нужен фикс
    @Test
    @Transactional
    public void updateBalance_happyPass() {
        //given
        UserEntity existEntity = entityUtil.addUserEntityToDb(UserRole.USER);
        UpdateBalanceDto updateBalanceDto = DtoUtil.createUpdateBalanceDto(existEntity.getId());

        //when
        UserFullDto userAfterUpdate = userService.updateBalance(updateBalanceDto);

        //then
        Assertions.assertEquals(userAfterUpdate.getBalance(), updateBalanceDto.getBalance());
    }
}
