package by.itstep.booking.services;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.CountryService;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class CountryServiceTest extends BookingApplicationTests {

    @Autowired
    private CountryService countryService;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserRepository userRepository;
    @MockBean
    AuthenticationService authenticationService;
    @Autowired
    private EntityUtil entityUtil;

    @Test
    @Transactional
    public void create_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryCreateDto createDto = DtoUtil.createCountryCreateDto();

        //when
        CountryDto dto = countryService.create(createDto);

        //then
        Assertions.assertNotNull(dto.getId());
        CountryEntity createdEntity = countryRepository.getById(dto.getId());
        Assertions.assertEquals(createdEntity.getName(), createDto.getName());
    }

    @Test
    @Transactional
    public void update_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryEntity existEntity = entityUtil.addCountryEntityToDb();
        CountryUpdateDto updateDto = DtoUtil.createCountryUpdateDto(existEntity.getId());

        //when
        CountryDto dtoAfterUpdated = countryService.update(updateDto);

        //then
        Assertions.assertEquals(existEntity.getId(), updateDto.getId());
        CountryEntity updatedEntity = countryRepository.getById(dtoAfterUpdated.getId());
        Assertions.assertEquals(updateDto.getName(), updatedEntity.getName());
    }

    @Test
    @Transactional
    public void findById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryEntity existCountryEntity = entityUtil.addCountryEntityToDb();;

        //when
        CountryDto foundDto = countryService.findById(existCountryEntity.getId());

        //then
        Assertions.assertEquals(existCountryEntity.getName(), foundDto.getName());
    }

    @Test
    @Transactional
    public void findAll_pappyPath() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        entityUtil.addCountryEntityToDb();
        entityUtil.addCountryEntityToDb();
        entityUtil.addCountryEntityToDb();

        //when
        Page<CountryDto> foundDtos = countryService.findAll(1, 3);

        //then
        Assertions.assertEquals(3, foundDtos.getTotalElements());
    }


    @Test
    @Transactional
    public void deleteById_happyPass() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryEntity existCountryEntity = entityUtil.addCountryEntityToDb();

        //when
        countryService.deleteById(existCountryEntity.getId());

        //then
        CountryEntity deletedEntity = countryRepository.getById(existCountryEntity.getId());
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
    }

    @Test
    @Transactional
    public void deleteById_happyPass_when_several_hotels() {
        //given
        UserEntity userEntity = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        Mockito.when(authenticationService.getAuthenticationUser())
                .thenReturn(userEntity);
        CountryEntity createdCountry = entityUtil.addCountryEntityToDb();
        HotelEntity hotel1 = entityUtil.addHotelEntityToDb(createdCountry);
        HotelEntity hotel2 = entityUtil.addHotelEntityToDb(createdCountry);
        RoomEntity room = entityUtil.addRoomEntityToDb(hotel1);


        //when
        countryService.deleteById(createdCountry.getId());

        //then
        CountryEntity deletedEntity = countryRepository.getById(createdCountry.getId());
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
        HotelEntity deleteHotel1 = hotelRepository.getById(hotel1.getId());
        Assertions.assertNotNull(deleteHotel1.getDeletedAt());
        HotelEntity deleteHotel2 = hotelRepository.getById(hotel2.getId());
        Assertions.assertNotNull(deleteHotel2.getDeletedAt());
        RoomEntity deletedRoom = roomRepository.getById(room.getId());
        Assertions.assertNotNull(deletedRoom.getDeletedAt());

    }
}
