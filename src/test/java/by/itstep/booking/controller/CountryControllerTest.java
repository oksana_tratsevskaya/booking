package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CountryControllerTest extends BookingApplicationTests {

    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EntityUtil entityUtil;


    @Test
    public void create_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        CountryCreateDto createDto = DtoUtil.createCountryCreateDto();

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/country")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto))
                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenNotAuthenticated() throws Exception{
        //given
        CountryCreateDto createDto = DtoUtil.createCountryCreateDto();

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/country")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        CountryCreateDto createDto = DtoUtil.createCountryCreateDto();

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/country")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }



    @Test
    public void update_happyPath() throws Exception {
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();
        CountryUpdateDto updateDto = DtoUtil.createCountryUpdateDto(existCountry.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/country")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticated() throws Exception {
        //given
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();
        CountryUpdateDto updateDto = DtoUtil.createCountryUpdateDto(existCountry.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/country")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/country/" + existCountry.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        mockMvc.perform(request(GET, "/country/" + existCountry.getId())
                .header("Authorization", "token"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());


        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/booking" )
                        .param("page", "0")
                        .param("size", "10")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticated() throws Exception{
        //given
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/country" )
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_happyPath() throws Exception {
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/country/" + existCountry.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAdmin() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.USER);
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/country/" + existCountry.getId())
                .header("Authorization", token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAuthenticated() throws Exception{
        //given
        CountryEntity existCountry = entityUtil.addCountryEntityToDb();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/country/" + existCountry.getId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
