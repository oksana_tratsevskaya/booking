package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.select.AvailableHotelsRequest;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class HotelControllerTest extends BookingApplicationTests {

    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EntityUtil entityUtil;


    @Test
    public void create_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        CountryEntity countryEntityToDb = entityUtil.addCountryEntityToDb();
        HotelCreateDto createDto = DtoUtil.createHotelCreateDto(countryEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/hotels")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto))
                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenNotAuthenticated() throws Exception{
        //given
        CountryEntity countryEntityToDb = entityUtil.addCountryEntityToDb();
        HotelCreateDto createDto = DtoUtil.createHotelCreateDto(countryEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/hotels")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.USER);
        CountryEntity countryEntityToDb = entityUtil.addCountryEntityToDb();
        HotelCreateDto createDto = DtoUtil.createHotelCreateDto(countryEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/hotels")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();
        HotelUpdateDto updateDto = DtoUtil.createHotelUpdateDto(existHotel.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/hotels")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticated() throws Exception {
        //given
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();
        HotelUpdateDto updateDto = DtoUtil.createHotelUpdateDto(existHotel.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/hotels")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/hotels/" + existHotel.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        mockMvc.perform(request(GET, "/hotels/" + existHotel.getId())
                .header("Authorization", "token"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        HotelEntity hotelEntity = entityUtil.addHotelToDbWithAddictedEntity();


        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/hotels" )
                        .param("page", "0")
                        .param("size", "10")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAvailableHotels_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        CountryEntity existCountry  = entityUtil.addCountryEntityToDb();
        HotelEntity existHotel = entityUtil.addHotelEntityToDb(existCountry);
        entityUtil.addRoomEntityToDb(existHotel);
        AvailableHotelsRequest request = DtoUtil.createAvailableHotelsRequest(existCountry);

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(POST, "/hotels/available")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticated() throws Exception{
        //given
        entityUtil.addHotelToDbWithAddictedEntity();
        entityUtil.addHotelToDbWithAddictedEntity();
        entityUtil.addHotelToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/hotels" )
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/hotels/" + existHotel.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAdmin() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.USER);
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/hotels/" + existHotel.getId())
                .header("Authorization", token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAuthenticated() throws Exception{
        //given
        HotelEntity existHotel = entityUtil.addHotelToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/hotels/" + existHotel.getId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
