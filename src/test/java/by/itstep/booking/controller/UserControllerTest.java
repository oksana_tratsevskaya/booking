package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest extends BookingApplicationTests {

    @Autowired
    private EntityUtil entityUtil;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void create_happyPath() throws Exception{
        //given
        UserCreteDto createDto = DtoUtil.creteUserCreateDto();

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/users")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(createDto))
        )
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(createDto.getEmail(), foundUser.getEmail());
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserUpdateDto updateDto = DtoUtil.createUserUpdateDto(currentUser.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticated() throws Exception {
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        UserUpdateDto updateDto = DtoUtil.createUserUpdateDto(currentUser.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception{
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/users/" + existingUser.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
    }



    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);

        //when
        mockMvc.perform(request(GET, "/users" + existingUser.getId())
                .header("Authorization", "token"))
                .andExpect(status().isForbidden());
    }



    @Test
    public void findAll_happyPath() throws Exception{
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/users" )
                        .param("page", "0")
                        .param("size", "10")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticated() throws Exception{
        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/users" )
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_happyPath() throws Exception{
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/users/" + existingUser.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAdmin() throws Exception{
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/users/" + existingUser.getId())
                .header("Authorization", token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAuthenticated() throws Exception{
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/users/" + existingUser.getId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void changePassword_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        ChangeUserPasswordDto dto = DtoUtil.createChangeUserPasswordDto(currentUser.getId(), currentUser.getPassword());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/changePassword")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void changeRole_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        ChangeUserRoleDto dto = DtoUtil.createChangeUserRoleDto(existingUser.getId(), UserRole.ADMIN);

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/role/" )
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void changeRole_whenNotAdmin() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        ChangeUserRoleDto dto = DtoUtil.createChangeUserRoleDto(currentUser.getId(), UserRole.ADMIN);

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/role")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void changeRole_whenNotAuthenticated() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        ChangeUserRoleDto dto = DtoUtil.createChangeUserRoleDto(currentUser.getId(), UserRole.ADMIN);

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/role")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void updateBalance_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UpdateBalanceDto dto = DtoUtil.createUpdateBalanceDto(currentUser.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/updateBalance" )
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void updateBalance_whenNotAuthenticated() throws Exception{
        //given
        UserEntity userInDb = entityUtil.addUserEntityToDb(UserRole.USER);
        UpdateBalanceDto dto = DtoUtil.createUpdateBalanceDto(userInDb.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users/updateBalance")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void blocked_happyPath() throws Exception {
        //given
        UserEntity existingUser = entityUtil.addUserEntityToDb(UserRole.USER);
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(PUT, "/users/" + existingUser.getId()+ "/blocked")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }











}
