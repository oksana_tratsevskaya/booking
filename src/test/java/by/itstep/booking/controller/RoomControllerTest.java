package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.webservices.client.AutoConfigureMockWebServiceServer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RoomControllerTest extends BookingApplicationTests {

    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EntityUtil entityUtil;


    @Test
    public void create_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomCreateDto createDto = DtoUtil.createRoomCreateDto(hotelEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/rooms")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto))
                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenNotAuthenticated() throws Exception{
        //given
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomCreateDto createDto = DtoUtil.createRoomCreateDto(hotelEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/rooms")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.USER);
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomCreateDto createDto = DtoUtil.createRoomCreateDto(hotelEntityToDb.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/rooms")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }



    @Test
    public void update_happyPath() throws Exception {
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();
        RoomUpdateDto updateDto = DtoUtil.createRoomUpdateDto(existRoom.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/rooms")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticated() throws Exception {
        //given
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();
        RoomUpdateDto updateDto = DtoUtil.createRoomUpdateDto(existRoom.getId());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/rooms")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(updateDto))
        )
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/rooms/" + existRoom.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();

        //when
        mockMvc.perform(request(GET, "/rooms/" + existRoom.getId())
                .header("Authorization", "token"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        entityUtil.addRoomToDbWithAddictedEntity();
        entityUtil.addRoomToDbWithAddictedEntity();


        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/rooms" )
                        .param("page", "0")
                        .param("size", "10")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticated() throws Exception{
        //given
        entityUtil.addRoomToDbWithAddictedEntity();
        entityUtil.addRoomToDbWithAddictedEntity();
        entityUtil.addRoomToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/rooms" )
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_happyPath() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.ADMIN);
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/rooms/" + existRoom.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAdmin() throws Exception{
        //given
        String token = jwtHelper.takeJwtToken(UserRole.USER);
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/rooms/" + existRoom.getId())
                .header("Authorization", token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAuthenticated() throws Exception{
        //given
        RoomEntity existRoom= entityUtil.addRoomToDbWithAddictedEntity();

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/rooms/" + existRoom.getId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
