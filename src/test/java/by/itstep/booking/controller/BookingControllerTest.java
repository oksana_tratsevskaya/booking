package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.util.DtoUtil;
import by.itstep.booking.util.EntityUtil;
import by.itstep.booking.util.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookingControllerTest extends BookingApplicationTests {

    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EntityUtil entityUtil;


    @Test
    public void create_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomEntity roomEntity =entityUtil.addRoomEntityToDb(hotelEntityToDb);
        BookingCreateDto createDto = DtoUtil.createBookingDto(hotelEntityToDb.getId(), currentUser.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/booking")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto))
                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenNotAuthenticated() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.ADMIN);
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomEntity roomEntity =entityUtil.addRoomEntityToDb(hotelEntityToDb);
        BookingCreateDto createDto = DtoUtil.createBookingDto(hotelEntityToDb.getId(), currentUser.getId());


        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/booking")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        HotelEntity hotelEntityToDb = entityUtil.addHotelToDbWithAddictedEntity();
        RoomEntity roomEntity =entityUtil.addRoomEntityToDb(hotelEntityToDb);
        BookingCreateDto createDto = DtoUtil.createBookingDto(hotelEntityToDb.getId(), currentUser.getId());

        //when
        MvcResult mvcResult = mockMvc
                .perform(request(POST, "/booking")
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        BookingEntity existBooking = entityUtil.addBookingToDbWithAddictedEntity(currentUser);

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/booking/" + existBooking.getId())
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        BookingEntity existBooking = entityUtil.addBookingToDbWithAddictedEntity(currentUser);

        //when
        mockMvc.perform(request(GET, "/booking/" + existBooking.getId())
                .header("Authorization", "token"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findAll_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        entityUtil.addBookingToDbWithAddictedEntity(currentUser);
        entityUtil.addBookingToDbWithAddictedEntity(currentUser);


        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/booking" )
                        .param("page", "0")
                        .param("size", "10")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticated() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        entityUtil.addBookingToDbWithAddictedEntity(currentUser);
        entityUtil.addBookingToDbWithAddictedEntity(currentUser);

        //when
        MvcResult mvcResult = mockMvc.perform(
                request(GET, "/booking" )
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteById_happyPath() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        BookingEntity existBooking = entityUtil.addBookingToDbWithAddictedEntity(currentUser);

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/booking/" + existBooking.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteById_whenNotAuthenticated() throws Exception{
        //given
        UserEntity currentUser = entityUtil.addUserEntityToDb(UserRole.USER);
        BookingEntity existBooking = entityUtil.addBookingToDbWithAddictedEntity(currentUser);

        //when
        MvcResult mvcResult = mockMvc.perform(request(DELETE, "/booking/" + existBooking.getId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
