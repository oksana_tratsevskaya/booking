package by.itstep.booking;

import by.itstep.booking.util.DbCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookingApplicationTests {

	public static final Faker FAKER = new Faker();

	@Autowired
	private DbCleaner dbCleaner;

	@BeforeEach
	public void setUp() {
		dbCleaner.clean();
	}

	@AfterEach
	public void shutDown() {
		dbCleaner.clean();
	}
}
